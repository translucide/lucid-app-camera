import Vue, { PluginFunction, VueConstructor } from 'vue';

interface InstallFunction extends PluginFunction<any> {
  installed?: boolean;
}

declare const LucidAppCamera: { install: InstallFunction };
export default LucidAppCamera;

export const LucidAppCameraSample: VueConstructor<Vue>;
