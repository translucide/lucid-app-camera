import Bowser from 'bowser';

export const isFileInputCamera = () => {
  const browser = (Bowser.getParser(window.navigator.userAgent) as any).parsedResult;
  const isIOS = browser.os.name === 'iOS' ? true : false;
  const isChromeOrFF = browser.browser.name === 'Chrome' || browser.browser.name === 'Firefox' ? true : false;
  const isStandalone = (window.navigator as any).standalone;

  if (isIOS && (isStandalone || isChromeOrFF)) {
    return true;
  } else {
    return false;
  }
};
